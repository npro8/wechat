<?php

namespace WePayV3;

use WePayV3\Contracts\BasicWePay;

/**
 * 商户进件
 * Class Applyment
 * @package WePayV3
 */
class Applyment extends BasicWePay
{
    /**
     * 上传图片
     * @param array $data 图片参数
     * @return array
     * @throws InvalidResponseException
     */
    public function upload($data)
    {
        return $this->uploadDoRequest('POST', '/v3/merchant/media/upload', $data);
    }

    /**
     * 数据加密
     * @param array $content 加密数据
     * @return array
     * @throws InvalidResponseException
     */
    public function rsa($content)
    {
        return $this->rsaEncode($content);
    }

    /**
     * 创建申请单
     * @param array $data 申请参数
     * @return array
     * @throws InvalidResponseException
     */
    public function create($data)
    {
        return $this->doRequest('POST', '/v3/applyment4sub/applyment/', json_encode($data, JSON_UNESCAPED_UNICODE), true);
    }

    /**
     * 申请单查询
     * @param string $applyment_id 单号
     * @return array
     * @throws InvalidResponseException
     */
    public function query($applyment_id)
    {
        $pathinfo = "/v3/applyment4sub/applyment/applyment_id/{$applyment_id}";
        return $this->doRequest('GET', "{$pathinfo}", '', true);
    }
}