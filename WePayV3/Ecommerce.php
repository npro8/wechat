<?php
namespace WePayV3;

use WeChat\Contracts\Tools;
use WeChat\Exceptions\InvalidArgumentException;
use WeChat\Exceptions\InvalidDecryptException;
use WeChat\Exceptions\InvalidResponseException;
use WePayV3\Contracts\BasicWePay;
use WePayV3\Contracts\DecryptAes;

/**
 * 平台收付通接口
 * Class Ecommerce
 * @package WePayV3
 */
class Ecommerce extends BasicWePay
{
    /**
     * 创建合单支付订单
     * @param string $type 支付类型
     * @param array $data 支付参数
     * @return array
     * @throws InvalidResponseException
     */
    public function create($type, $data)
    {
        $types = [
            'h5'     => '/v3/combine-transactions/h5',
            'app'    => '/v3/combine-transactions/app',
            'jsapi'  => '/v3/combine-transactions/jsapi',
            'native' => '/v3/combine-transactions/native',
        ];
        if (empty($types[$type])) {
            throw new InvalidArgumentException("Payment {$type} not defined.");
        } else {
            // 创建预支付码
            $result = $this->doRequest('POST', $types[$type], json_encode($data, JSON_UNESCAPED_UNICODE), true);
            if (empty($result['prepay_id'])) return $result;
            // 支付参数签名
            $time = (string)time();
            $appid = $this->config['mini_appid'];
            $prepayId = $result['prepay_id'];
            $nonceStr = Tools::createNoncestr();
            if ($type === 'app') {
                $sign = $this->signBuild(join("\n", [$appid, $time, $nonceStr, $prepayId, '']));
                return ['partnerId' => $this->config['mch_id'], 'prepayId' => $prepayId, 'package' => 'Sign=WXPay', 'nonceStr' => $nonceStr, 'timeStamp' => $time, 'sign' => $sign];
            } elseif ($type === 'jsapi') {
                $sign = $this->signBuild(join("\n", [$appid, $time, $nonceStr, "prepay_id={$prepayId}", '']));
                return ['appId' => $appid, 'timeStamp' => $time, 'nonceStr' => $nonceStr, 'package' => "prepay_id={$prepayId}", 'signType' => 'RSA', 'paySign' => $sign];
            } else {
                return $result;
            }
        }
    }

    /**
     * 创建单笔支付订单
     * @param string $type 支付类型
     * @param array $data 支付参数
     * @return array
     * @throws InvalidResponseException
     */
    public function create_one($type, $data)
    {
        $types = [
            'h5'     => '/v3/pay/partner/transactions/h5',
            'app'    => '/v3/pay/partner/transactions/app',
            'jsapi'  => '/v3/pay/partner/transactions/jsapi',
            'native' => '/v3/pay/partner/transactions/native',
        ];
        if (empty($types[$type])) {
            throw new InvalidArgumentException("Payment {$type} not defined.");
        } else {
            // 创建预支付码
            $result = $this->doRequest('POST', $types[$type], json_encode($data, JSON_UNESCAPED_UNICODE), true);
            if (empty($result['prepay_id'])) return $result;
            // 支付参数签名
            $time = (string)time();
            $appid = $this->config['mini_appid'];
            $prepayId = $result['prepay_id'];
            $nonceStr = Tools::createNoncestr();
            if ($type === 'app') {
                $sign = $this->signBuild(join("\n", [$appid, $time, $nonceStr, $prepayId, '']));
                return ['partnerId' => $this->config['mch_id'], 'prepayId' => $prepayId, 'package' => 'Sign=WXPay', 'nonceStr' => $nonceStr, 'timeStamp' => $time, 'sign' => $sign];
            } elseif ($type === 'jsapi') {
                $sign = $this->signBuild(join("\n", [$appid, $time, $nonceStr, "prepay_id={$prepayId}", '']));
                return ['appId' => $appid, 'timeStamp' => $time, 'nonceStr' => $nonceStr, 'package' => "prepay_id={$prepayId}", 'signType' => 'RSA', 'paySign' => $sign];
            } else {
                return $result;
            }
        }
    }

    /**
     * 合单订单查询
     * @param string $orderNo 订单单号
     * @return array
     * @throws InvalidResponseException
     */
    public function query($orderNo)
    {
        $pathinfo = "/v3/combine-transactions/out-trade-no/{$orderNo}";
        return $this->doRequest('GET', "{$pathinfo}", '', true);
    }

    /**
     * 单笔订单查询
     * @param string $id 订单单号
     * @param int $type 1: 订单单号，2：微信订单号
     * @return array
     * @throws InvalidResponseException
     */
    public function query_one($id, $type = 1)
    {
        $pathinfo = $type == 1 ? "/v3/pay/partner/transactions/out-trade-no/{$id}" : "/v3/pay/partner/transactions/id/{$orderNo}";
        return $this->doRequest('GET', "{$pathinfo}?sp_mchid{$this->config['sp_mch_id']}=&sub_mchid={$this->config['mch_id']}", '', true);
    }

    /**
     * 创建退款订单
     * @param array $data 退款参数
     * @return array
     * @throws InvalidResponseException
     */
    public function refund($data)
    {
        return $this->doRequest('POST', '/v3/ecommerce/refunds/apply', json_encode($data, JSON_UNESCAPED_UNICODE), true);
    }

    /**
     * 退款订单查询
     * @param string $no 单号
     * @param int $type 1: 退款单号，2：微信订单号
     * @return array
     * @throws InvalidResponseException
     */
    public function refund_query($no, $type = 1)
    {
        $pathinfo = $type == 1 ? "/v3/ecommerce/refunds/out-refund-no/{$no}" : "/v3/ecommerce/refunds/id/{$no}";
        return $this->doRequest('GET', "{$pathinfo}?sub_mchid={$this->config['mch_id']}", '', true);
    }

    /**
     * 支付通知解密
     * @return array
     * @throws InvalidDecryptException
     */
    public function notify()
    {
        $body = file_get_contents('php://input');
        $data = json_decode($body, true);
        if (isset($data['resource'])) {
            $aes = new DecryptAes($this->config['mch_v3_key']);
            $data['result'] = $aes->decryptToString(
                $data['resource']['associated_data'],
                $data['resource']['nonce'],
                $data['resource']['ciphertext']
            );
        }
        return $data;
    }

    /**
     * 支付验证内容签名
     * @param string $post 签名内容
     * @param string $header 数据头
     * @return int
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function paySignVerify($post, $header)
    {
        $data =
            $header['wechatpay-timestamp'] . "\n" .
            $header['wechatpay-nonce'] . "\n" .
            $post . "\n";

        $sign = $header['wechatpay-signature'];

        $cert = $this->tmpFile($header['wechatpay-serial']);
        if (empty($cert)) {
            Cert::instance($this->config)->download();
            $cert = $this->tmpFile($header['wechatpay-serial']);
        }

        return @openssl_verify($data, base64_decode($sign), openssl_x509_read($cert), 'sha256WithRSAEncryption');
    }

    /**
     * 进件素材上传
     * @param array $data 素材参数
     * @return array
     * @throws InvalidResponseException
     */
    public function upload($data)
    {
        return $this->uploadDoRequest('POST', '/v3/merchant/media/upload', $data);
    }

    /**
     * 参数加密
     * @param string $content 素材参数
     * @return string
     * @throws InvalidResponseException
     */
    public function rsa($content)
    {
        return $this->rsaEncode($content);
    }

    /**
     * 创建进件申请单
     * @param array $data 申请参数
     * @return array
     * @throws InvalidResponseException
     */
    public function applyment($data)
    {
        return $this->doRequest_applyment('POST', '/v3/ecommerce/applyments/', json_encode($data, JSON_UNESCAPED_UNICODE), true);
    }

    /**
     * 进件申请单查询
     * @param string $applyment_id 单号
     * @return array
     * @throws InvalidResponseException
     */
    public function applyment_query($applyment_id)
    {
        $pathinfo = "/v3/ecommerce/applyments/{$applyment_id}";
        return $this->doRequest('GET', "{$pathinfo}", '', true);
    }

    /**
     * 创建分账接收方
     * @param array $data 接收方参数
     * @return array
     * @throws InvalidResponseException
     */
    public function receivers_add($data)
    {
        if (isset($data['name'])) $data['name'] = $this->rsaEncode($data['name']);
        return $this->doRequest('POST', '/v3/ecommerce/profitsharing/receivers/add', json_encode($data, JSON_UNESCAPED_UNICODE), true);
    }

    /**
     * 创建分账
     * @param array $data 分账参数
     * @return array
     * @throws InvalidResponseException
     */
    public function profitsharing($data)
    {
        foreach ($data['receivers'] as &$v) if (isset($v['name'])) $v['name'] = $this->rsaEncode($v['name']);
        return $this->doRequest('POST', '/v3/ecommerce/profitsharing/orders', json_encode($data, JSON_UNESCAPED_UNICODE), true);
    }

    /**
     * 完结分账
     * @param array $data 解冻参数
     * @return array
     * @throws InvalidResponseException
     */
    public function finish($data)
    {
        return $this->doRequest('POST', '/v3/ecommerce/profitsharing/finish-order', json_encode($data, JSON_UNESCAPED_UNICODE), true);
    }

    /**
     * 分账订单查询
     * @param string $orderNo 订单单号
     * @param string $transaction_id 微信单号
     * @return array
     * @throws InvalidResponseException
     */
    public function profitsharing_query($orderNo, $transaction_id)
    {
        $pathinfo = "/v3/ecommerce/profitsharing/orders";
        return $this->doRequest('GET', "{$pathinfo}?sub_mchid={$this->config['sub_mch_id']}&transaction_id={$transaction_id}&out_order_no={$orderNo}", '', true);
    }

    /**
     * 分账订单剩余待分金额查询
     * @param string $transaction_id 微信单号
     * @return array
     * @throws InvalidResponseException
     */
    public function profitsharing_amounts_query($transaction_id)
    {
        $pathinfo = "/v3/ecommerce/profitsharing/orders/{$transaction_id}/amounts";
        return $this->doRequest('GET', "{$pathinfo}", '', true);
    }
}
