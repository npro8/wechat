<?php
namespace WePayV3;

use WePayV3\Contracts\BasicWePay;

/**
 * 普通商户商家转账到零钱
 * @class Transfers
 * @package WePayV3
 */
class Transfers extends BasicWePay
{
    /**
     * 发起商家批量转账
     * @param array $body
     * @return array
     * @throws \WeChat\Exceptions\InvalidDecryptException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @link https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter4_3_1.shtml
     */
    public function create($body)
    {
        if (!isset($body['appid']) || empty($body['appid'])) {
            $body['appid'] = $this->config['appid'];
        }
        if (isset($body['transfer_detail_list']) && is_array($body['transfer_detail_list'])) {
            foreach ($body['transfer_detail_list'] as &$item) if (isset($item['user_name'])) {
                $item['user_name'] = $this->rsaEncode($item['user_name']);
            }
        }
        if (empty($body['total_num'])) {
            $body['total_num'] = count($body['transfer_detail_list']);
        }
        return $this->doRequest('POST', '/v3/transfer/batches', json_encode($body, JSON_UNESCAPED_UNICODE), true);
    }

    /**
     * 通过微信批次单号查询批次单
     * @param string $batchId 微信批次单号(二选一)
     * @param string $outBatchNo 商家批次单号(二选一)
     * @param boolean $needQueryDetail 查询指定状态
     * @param integer $offset 请求资源的起始位置
     * @param integer $limit 最大明细条数
     * @param string $detailStatus 查询指定状态
     * @return array
     * @throws \WeChat\Exceptions\InvalidResponseException
     */
    public function query($batchId = '', $outBatchNo = '', $needQueryDetail = true, $offset = 0, $limit = 20, $detailStatus = 'ALL')
    {
        if (empty($batchId)) {
            $pathinfo = "/v3/transfer/batches/out-batch-no/{$outBatchNo}";
        } else {
            $pathinfo = "/v3/transfer/batches/batch-id/{$batchId}";
        }
        $params = http_build_query([
            'limit'             => $limit,
            'offset'            => $offset,
            'detail_status'     => $detailStatus,
            'need_query_detail' => $needQueryDetail ? 'true' : 'false',
        ]);
        return $this->doRequest('GET', "{$pathinfo}?{$params}", '', true);
    }

    /**
     * 通过商家明细单号查询明细单
     * @param string $outBatchNo 商户系统内部的商家批次单号，在商户系统内部唯一
     * @param string $outDetailNo 商户系统内部区分转账批次单下不同转账明细单的唯一标识
     * @return array
     * @throws \WeChat\Exceptions\InvalidResponseException
     */
    public function detailOutBatchNo($outBatchNo, $outDetailNo)
    {
        $pathinfo = "/v3/transfer/batches/out-batch-no/{$outBatchNo}/details/out-detail-no/{$outDetailNo}";
        return $this->doRequest('GET', $pathinfo, '', true);
    }

    /**
     * 通过微信明细单号查询明细单
     * @param string $batchId 微信批次单号
     * @param string $detailId 微信支付系统内部区分转账批次单下不同转账明细单的唯一标识
     * @return array
     * @throws \WeChat\Exceptions\InvalidResponseException
     */
    public function detailBatchId($batchId, $detailId)
    {
        $pathinfo = "/v3/transfer/batches/batch-id/{$batchId}/details/detail-id/{$detailId}";
        return $this->doRequest('GET', $pathinfo, '', true);
    }

    /**
     * 错误码
     * @param string $code 错误编码
     * @return string
     */
    public static function reason($code)
    {
        $arr = [
            'ACCOUNT_FROZEN' => '该用户账户被冻结',
            'REAL_NAME_CHECK_FAIL' => '收款人未实名认证，需要用户完成微信实名认证',
            'NAME_NOT_CORRECT' => '收款人姓名校验不通过，请核实信息',
            'OPENID_INVALID' => 'Openid格式错误或者不属于商家公众账号',
            'TRANSFER_QUOTA_EXCEED' => '超过用户单笔收款额度，核实产品设置是否准确',
            'DAY_RECEIVED_QUOTA_EXCEED' => '超过用户单日收款额度，核实产品设置是否准确',
            'MONTH_RECEIVED_QUOTA_EXCEED' => '超过用户单月收款额度，核实产品设置是否准确',
            'DAY_RECEIVED_COUNT_EXCEED' => '超过用户单日收款次数，核实产品设置是否准确',
            'PRODUCT_AUTH_CHECK_FAIL' => '未开通该权限或权限被冻结，请核实产品权限状态',
            'OVERDUE_CLOSE' => '超过系统重试期，系统自动关闭',
            'ID_CARD_NOT_CORRECT' => '收款人身份证校验不通过，请核实信息',
            'ACCOUNT_NOT_EXIST' => '该用户账户不存在',
            'TRANSFER_RISK' => '该笔转账可能存在风险，已被微信拦截',
            'OTHER_FAIL_REASON_TYPE' => '其它失败原因',
            'REALNAME_ACCOUNT_RECEIVED_QUOTA_EXCEED' => '请引导用户在微信支付查看详情',
            'RECEIVE_ACCOUNT_NOT_PERMMIT' => '请在产品设置中调整，添加该用户为收款人',
            'PAYEE_ACCOUNT_ABNORMAL' => '请联系用户完善其在微信支付的身份信息以继续收款',
            'PAYER_ACCOUNT_ABNORMAL' => '可前往商户平台获取解除功能限制指引',
            'TRANSFER_SCENE_UNAVAILABLE' => '该转账场景暂不可用，请确认转账场景ID是否正确',
            'TRANSFER_SCENE_INVALID' => '你尚未获取该转账场景，请确认转账场景ID是否正确',
            'TRANSFER_REMARK_SET_FAIL' => '转账备注设置失败， 请调整后重新再试',
            'RECEIVE_ACCOUNT_NOT_CONFIGURE' => '请前往商户平台-商家转账到零钱-前往功能-转账场景中添加',
            'BLOCK_B2C_USERLIMITAMOUNT_BSRULE_MONTH' => '超出用户单月转账收款20w限额，本月不支持继续向该用户付款',
            'BLOCK_B2C_USERLIMITAMOUNT_MONTH' => '用户账户存在风险收款受限，本月不支持继续向该用户付款',
            'MERCHANT_REJECT' => '商户员工（转账验密人）已驳回转账',
            'MERCHANT_NOT_CONFIRM' => '商户员工（转账验密人）超时未验密'
        ];
        return $arr[$code] ?? '';
    }
}
