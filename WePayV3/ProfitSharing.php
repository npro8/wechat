<?php
namespace WePayV3;

use WePayV3\Contracts\BasicWePay;

/**
 * 普通商户商家分账
 * Class Profitsharing
 * @package WePayV3
 */
class ProfitSharing extends BasicWePay
{
    /**
     * 请求分账
     * @param array $options
     * @return array
     * @throws \WeChat\Exceptions\InvalidResponseException
     */
    public function create($options)
    {
        if (!isset($options['appid'])) $options['appid'] = $this->config['appid'];
        foreach ($options['receivers'] as &$v) if (isset($v['name'])) $v['name'] = $this->rsaEncode($v['name']);
        return $this->doRequest('POST', '/v3/profitsharing/orders', json_encode($options, JSON_UNESCAPED_UNICODE), true);
    }


    /**
     * 查询分账结果
     * @param string $outOrderNo 商户分账单号
     * @param string $transactionId 微信订单号
     * @return array
     * @throws \WeChat\Exceptions\InvalidResponseException
     */
    public function query($outOrderNo, $transactionId)
    {
        $pathinfo = "/v3/profitsharing/orders/{$outOrderNo}?&transaction_id={$transactionId}";
        return $this->doRequest('GET', $pathinfo, '', true);
    }

    /**
     * 分账订单查询
     * @param string $orderNo 订单单号
     * @param string $transaction_id 微信单号
     * @return array
     * @throws InvalidResponseException
     */
    public function spQuery($outOrderNo, $transactionId)
    {
        $pathinfo = "/v3/profitsharing/orders/{$outOrderNo}?&sub_mchid={$this->config['sub_mch_id']}&transaction_id={$transactionId}";
        return $this->doRequest('GET', $pathinfo, '', true);
    }

    /**
     * 解冻剩余资金
     * @param array $options
     * @return array
     * @throws \WeChat\Exceptions\InvalidResponseException
     */
    public function unfreeze($options)
    {
        return $this->doRequest('POST', '/v3/profitsharing/orders/unfreeze', json_encode($options, JSON_UNESCAPED_UNICODE), true);
    }

    /**
     * 查询剩余待分金额
     * @param string $transactionId 微信订单号
     * @return array
     * @throws \WeChat\Exceptions\InvalidResponseException
     */
    public function amounts($transactionId)
    {
        $pathinfo = "/v3/profitsharing/transactions/{$transactionId}/amounts";
        return $this->doRequest('GET', $pathinfo, '', true);
    }

    /**
     * 添加分账接收方
     * @param array $options
     * @return array
     * @throws \WeChat\Exceptions\InvalidResponseException
     */
    public function addReceiver($options)
    {
        if (!isset($options['appid'])) $options['appid'] = $this->config['appid'];
        if (isset($options['name'])) $options['name'] = $this->rsaEncode($options['name']);
        return $this->doRequest('POST', "/v3/profitsharing/receivers/add", json_encode($options, JSON_UNESCAPED_UNICODE), true);
    }

    /**
     * 删除分账接收方
     * @param array $options
     * @return array
     * @throws \WeChat\Exceptions\InvalidResponseException
     */
    public function deleteReceiver($options)
    {
        if (!isset($options['appid'])) $options['appid'] = $this->config['appid'];
        return $this->doRequest('POST', "/v3/profitsharing/receivers/delete", json_encode($options, JSON_UNESCAPED_UNICODE), true);
    }
}
