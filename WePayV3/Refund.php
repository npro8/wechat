<?php
namespace WePayV3;

use WeChat\Contracts\Tools;
use WeChat\Exceptions\InvalidDecryptException;
use WeChat\Exceptions\InvalidResponseException;
use WePayV3\Contracts\BasicWePay;

/**
 * 订单退款接口
 * 注意：与Order中的一致
 * @deprecated
 * @class Refund
 * @package WePayV3
 */
class Refund extends BasicWePay
{
    /**
     * 创建退款订单(通用)
     * @param array $data 退款参数
     * @return array
     * @throws InvalidResponseException
     */
    public function create($data)
    {
        return $this->doRequest('POST', '/v3/refund/domestic/refunds', json_encode($data, JSON_UNESCAPED_UNICODE), true);
        // return Order::instance($this->config)->createRefund($data);
    }

    /**
     * 退款订单查询(通用)
     * @param string $refundNo 退款单号
     * @return array
     * @throws InvalidResponseException
     */
    public function query($refundNo)
    {
        return $this->doRequest('GET', "/v3/refund/domestic/refunds/{$refundNo}", '', true);
        // return Order::instance($this->config)->queryRefund($refundNo);
    }

    /**
     * 创建退款订单(平台收付通)(电商)
     * @param array $data 退款参数
     * @return array
     * @throws InvalidResponseException
     */
    public function refund($data)
    {
        return $this->doRequest('POST', '/v3/ecommerce/refunds/apply', json_encode($data, JSON_UNESCAPED_UNICODE), true);
    }

    /**
     * 退款订单查询(平台收付通)(电商)
     * @param string $refundNo 退款单号
     * @return array
     * @throws InvalidResponseException
     */
    public function queryRefund($refundNo)
    {
        $pathinfo = "/v3/ecommerce/refunds/out-refund-no/{$refundNo}";
        return $this->doRequest('GET', "{$pathinfo}?sub_mchid={$this->config['mch_id']}", '', true);
    }

    /**
     * 获取退款通知
     * @param mixed $xml
     * @return array
     * @throws \WeChat\Exceptions\InvalidDecryptException
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function notify($xml = [])
    {
        return Order::instance($this->config)->notifyRefund($xml);
        // (平台收付通)(电商) ??
        // $data = Tools::xml2arr(empty($xml) ? Tools::getRawInput() : $xml);
        // if (!isset($data['return_code']) || $data['return_code'] !== 'SUCCESS') {
        //     throw new InvalidResponseException('获取退款通知XML失败！');
        // }
        // try {
        //     $key = md5($this->config['mch_v3_key']);
        //     $decrypt = base64_decode($data['req_info']);
        //     $response = openssl_decrypt($decrypt, 'aes-256-ecb', $key, OPENSSL_RAW_DATA);
        //     $data['result'] = Tools::xml2arr($response);
        //     return $data;
        // } catch (\Exception $exception) {
        //     throw new InvalidDecryptException($exception->getMessage(), $exception->getCode());
        // }
    }

    /**
     * v3退款验证内容签名
     * @param string $post 签名内容
     * @param string $header 数据头
     * @return int
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function refundSignVerify($post, $header)
    {
        $data =
            $header['wechatpay-timestamp'] . "\n" .
            $header['wechatpay-nonce'] . "\n" .
            $post . "\n";

        return $this->signVerify($data, $header['wechatpay-signature'], $header['wechatpay-serial']);
    }
}