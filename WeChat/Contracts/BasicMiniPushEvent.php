<?php
namespace WeChat\Contracts;

use WeChat\Exceptions\InvalidArgumentException;
use WeChat\Exceptions\InvalidDecryptException;
use WeChat\Exceptions\InvalidResponseException;
use WeChat\Prpcrypt\Prpcrypt;

/**
 * 微信小程序通知处理基本类
 * Class BasicMiniPushEvent
 * @package WeChat\Contracts
 */
class BasicMiniPushEvent
{
    /**
     * 小程序APPID
     * @var string
     */
    protected $appid;

    /**
     * 小程序推送内容
     * @var string
     */
    protected $post;

    /**
     * 小程序推送加密类型
     * @var string
     */
    protected $encryptType;

    /**
     * 小程序的推送请求参数
     * @var DataArray
     */
    protected $input;

    /**
     * 当前小程序配置对象
     * @var DataArray
     */
    protected $config;

    /**
     * 小程序推送内容对象
     * @var DataArray
     */
    protected $receive;
    
    /**
     * 数据类型
     * @var String
     */
    protected $datatype;

    /**
     * BasicMiniPushEvent constructor.
     * @param array $options
     * @throws \WeChat\Exceptions\InvalidResponseException
     */
    public function __construct(array $options)
    {
        if (empty($options['appid'])) {
            throw new InvalidArgumentException("Missing Config -- [appid]");
        }
        if (empty($options['appsecret'])) {
            throw new InvalidArgumentException("Missing Config -- [appsecret]");
        }
        if (empty($options['token'])) {
            throw new InvalidArgumentException("Missing Config -- [token]");
        }
        // 参数初始化
        $this->config = new DataArray($options);
        $this->input = new DataArray($_REQUEST);
        $this->appid = $this->config->get('appid');
        $this->datatype = $this->config->get('datatype');
        // 推送消息处理
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $this->post = Tools::getRawInput();
            $this->encryptType = $this->input->get('encrypt_type');
            if ($this->isEncrypt()) {
                if (empty($options['encodingaeskey'])) {
                    throw new InvalidArgumentException("Missing Config -- [encodingaeskey]");
                }
                $prpcrypt = new Prpcrypt($this->config->get('encodingaeskey'));
                $result = $this->datatype == 'JSON' ? Tools::json2arr($this->post) : Tools::xml2arr($this->post);
                $array = $prpcrypt->decrypt($result['Encrypt']);
                if (intval($array[0]) > 0) {
                    throw new InvalidResponseException($array[1], $array[0]);
                }
                list($this->post, $this->appid) = [$array[1], $array[2]];
            }
            $this->receive = new DataArray($this->datatype == 'JSON' ? Tools::json2arr($this->post) : Tools::xml2arr($this->post));
        } elseif ($_SERVER['REQUEST_METHOD'] == "GET" && $this->checkSignature()) {
            @ob_clean();
            echo($this->input->get('echostr'));
            $this->receive = new DataArray([]);
        } else {
            throw new InvalidResponseException('Invalid interface request.', '0');
        }
    }

    /**
     * 消息是否需要加密
     * @return boolean
     */
    public function isEncrypt()
    {
        return $this->encryptType === 'aes';
    }

    /**
     * 回复消息
     * @param array $data 消息内容
     * @param boolean $return 是否返回XML内容
     * @param boolean $isEncrypt 是否加密内容
     * @return string
     * @throws \WeChat\Exceptions\InvalidDecryptException
     */
    public function reply(array $data = [], $return = false, $isEncrypt = false)
    {
        $data = $this->datatype == 'JSON' ? Tools::arr2json($data) : Tools::arr2xml($data);
        if ($this->isEncrypt() || $isEncrypt) {
            $prpcrypt = new Prpcrypt($this->config->get('encodingaeskey'));
            $appid = $this->appid; $array = $prpcrypt->encrypt($data, $appid);
            if ($array[0] > 0) throw new InvalidDecryptException('Encrypt Error.', '0');
            list($timestamp, $encrypt) = [time(), $array[1]];
            $nonce = rand(77, 999) * rand(605, 888) * rand(11, 99);
            $tmpArr = [$this->config->get('token'), $timestamp, $nonce, $encrypt];
            sort($tmpArr, SORT_STRING);
            $signature = sha1(implode($tmpArr));
            if ($this->datatype == 'JSON') {
                $data = Tools::arr2json([
                    'Encrypt' => $encrypt,
                    'MsgSignature' => $signature,
                    'TimeStamp' => $timestamp,
                    'Nonce' => $nonce
                ]);
            } else {
                $format = "<xml><Encrypt><![CDATA[%s]]></Encrypt><MsgSignature><![CDATA[%s]]></MsgSignature><TimeStamp>%s</TimeStamp><Nonce><![CDATA[%s]]></Nonce></xml>";
                $data = sprintf($format, $encrypt, $signature, $timestamp, $nonce);
            }
        }
        if ($return) return $data;
        @ob_clean();
        echo $data;
    }

    /**
     * 验证来自微信服务器
     * @return bool
     */
    private function checkSignature()
    {
        $nonce = $this->input->get('nonce');
        $timestamp = $this->input->get('timestamp');
        $msg_signature = $this->input->get('msg_signature');
        $signature = empty($msg_signature) ? $this->input->get('signature') : $msg_signature;
        $tmpArr = [$this->config->get('token'), $timestamp, $nonce, ''];
        sort($tmpArr, SORT_STRING);
        return sha1(implode($tmpArr)) === $signature;
    }

    /**
     * 获取小程序推送对象
     * @param null|string $field 指定获取字段
     * @return array
     */
    public function getReceive($field = null)
    {
        return $this->receive->get($field);
    }
}