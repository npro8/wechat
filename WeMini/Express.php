<?php

namespace WeMini;

use WeChat\Contracts\BasicWeChat;
use WeChat\Exceptions\InvalidResponseException;
use WeChat\Exceptions\LocalCacheException;

/**
 * 小程序发货及物流
 * Class Express
 * @package WeMini
 */
class Express extends BasicWeChat
{
    /**
     * 获取运力id
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function deliveryList()
    {
        $url = 'https://api.weixin.qq.com/cgi-bin/express/delivery/open_msg/get_delivery_list?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, [], true);
    }

    /**
     * 发货录入
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function shipping($data)
    {
        $url = 'https://api.weixin.qq.com/wxa/sec/order/upload_shipping_info?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 消息跳转路径设置
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function jumpPath($path)
    {
        $url = 'https://api.weixin.qq.com/wxa/sec/order/set_msg_jump_path?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, ['path' => $path], true);
    }

    /**
     * 确认收货提醒
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function notifyConfirm($data)
    {
        $url = 'https://api.weixin.qq.com/wxa/sec/order/notify_confirm_receive?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 物流轨迹查询
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     * @数据注释
     * $data = [
            'openid' => '用户openid',
            'receiver_phone' => '收货手机号',
            'waybill_id' => '物流单号',
            'goods_info' => [
                "detail_list" => [
                    [
                        "goods_name" => '商品名称',
                        "goods_img_url" => $this->request->domain() . '商品图片'
                    ]
                ]
            ],
            // 'trans_id' => '交易单号',
            'order_detail_path' => '回调跳转路径'
        ];
     */
    public function logistics($data)
    {
        $url = 'https://api.weixin.qq.com/cgi-bin/express/delivery/open_msg/trace_waybill?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 发货合单录入
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function combinedShipping($data)
    {
        $url = 'https://api.weixin.qq.com/wxa/sec/order/upload_combined_shipping_info?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 查询订单发货状态
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function getOrder($data)
    {
        $url = 'https://api.weixin.qq.com/wxa/sec/order/get_order?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 查询订单发货状态
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function orderList($data)
    {
        $url = 'https://api.weixin.qq.com/wxa/sec/order/get_order_list?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, $data, true);
    }
}