<?php
namespace WeMini;

use WeChat\Contracts\BasicWeChat;

/**
 * 小程序内容安全
 * Class Security
 * @package WeMini
 */
class Security extends BasicWeChat
{
    /**
     * 检查一段文本是否含有违法违规内容
     * @param string $content
     * @param string $openid
     * @param int $scene
     * @param string $type
     * @return array
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function secCheck($content, $openid, $scene = 2, $type = 'msg')
    {
        if ($type == 'msg') {
            $url = 'https://api.weixin.qq.com/wxa/msg_sec_check?access_token=ACCESS_TOKEN';
            // 内容必须是UTF-8
            $data = "{\"openid\":\"".$openid."\",\"scene\":".$scene.",\"version\":2,\"content\":\"".$content."\"}";
            return $this->callPostApi($url, $data, false);
        } else {
            $url = 'https://api.weixin.qq.com/wxa/media_check_async?access_token=ACCESS_TOKEN';
            $data = [
                'media_url' => $content,
                'media_type' => $type, // 1:音频;2:图片
                'version' => 2,
                'scene' => $scene,
                'openid' => $openid
            ];
            return $this->callPostApi($url, $data, true);
        }
    }

    /**
     * 异步校验图片/音频是否含有违法违规内容(废弃)
     * @param string $media_url
     * @param string $media_type
     * @return array
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function mediaCheckAsync($media_url, $media_type)
    {
        $url = 'https://api.weixin.qq.com/wxa/media_check_async?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, ['media_url' => $media_url, 'media_type' => $media_type], true);
    }

    /**
     * 校验一张图片是否含有违法违规内容(废弃)
     * @param string $media 要检测的图片文件，格式支持PNG、JPEG、JPG、GIF，图片尺寸不超过 750px x 1334px
     * @return array
     * @throws \WeChat\Exceptions\InvalidResponseException
     * @throws \WeChat\Exceptions\LocalCacheException
     */
    public function imgSecCheck($media)
    {
        $url = 'https://api.weixin.qq.com/wxa/img_sec_check?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, ['media' => $media], false);
    }
}