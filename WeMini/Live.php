<?php
namespace WeMini;

use WeChat\Contracts\BasicWeChat;

/**
 * 小程序直播接口
 * Class Live
 * @package WeMini
 */
class Live extends BasicWeChat
{
    /**
     * 创建主播
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     * @param {"username": 微信号, "role": 1管理员2主播3运营者}
     * @return {"errcode": 0}
     */
    public function createUser($data)
    {
        $url = 'https://api.weixin.qq.com/wxaapi/broadcast/role/addrole?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 删除主播
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     * @param {"username": 微信号, "role": 1管理员2主播3运营者}
     * @return {"errcode": 0}
     */
    public function deleteUser($data)
    {
        $url = 'https://api.weixin.qq.com/wxaapi/broadcast/role/deleterole?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 创建直播间
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     * @param {name: "房间名字", coverImg: "背景图素材", startTime: "开始时间时间戳", endTime: "结束时间时间戳", anchorName: "主播昵称", anchorWechat: "主播微信号", subAnchorWechat: "主播副号微信号", createrWechat: '创建者微信号', shareImg: "分享图素材", feedsImg: "封面图素材", isFeedsPublic: 是否开启官方收录1开启0关闭, type: 直播类型1推流0手机直播, closeLike: 是否关闭点赞1关闭, closeGoods: 是否关闭商品货架1关闭, closeComment: 是否开启评论，1关闭, closeReplay: 是否关闭回放1关闭, closeShare: 是否关闭分享1关闭, closeKf: 是否关闭客服1关闭}
     * @return {"roomId": 房间id, "errcode": 0, "qrcode_url": "直播码, 当主播微信号没有在 “小程序直播“ 小程序实名认证 返回该字段"}
     */
    public function create($data)
    {
        $url = 'https://api.weixin.qq.com/wxaapi/broadcast/room/create?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 获取直播房间列表
     * @param integer $start 起始拉取房间
     * @param integer $limit 每次拉取的个数上限
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     * @param {"start": 索引, "limit": 个数50以内}
     * @return {"errcode": 0, "errmsg": "ok",  "total":总数, "room_info":[{"name":"直播房间名", "roomid": 房间id, "cover_img":"背景图", "share_img":"分享图", "live_status": 101直播中102未开始103已结束104禁播105暂停106异常107已过期, "start_time": 开始时间戳, "end_time": 结束时间戳, "anchor_name":"主播名", "goods":[{"cover_img":"商品图片", "url":"商品链接", "name":"商品名称", "price": 价格1, "price2": 价格2, "price_type": 1固定2区间, "goods_id": 商品id}], live_type: 直播类型1推流0手机直播, close_like: 是否关闭点赞1关闭, close_goods: 是否关闭商品货架1关闭, close_comment: 是否开启评论，1关闭, close_replay: 是否关闭回放1关闭, "close_kf": 是否关闭客服1关闭, "close_replay": 是否关闭回放0开启1关闭, "is_feeds_public": 是否开启官方收录1开启0关闭, "creater_openid": "创建者openid", "feeds_img": "封面图"}]}
     */
    public function getLiveList($start = 0, $limit = 10)
    {
        $url = 'https://api.weixin.qq.com/wxa/business/getliveinfo?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, ['start' => $start, 'limit' => $limit], true);
    }

    /**
     * 获取回放源视频
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     * @param {"action": "get_replay", "room_id": 房间id, "start": 0, "limit": 100以内}
     * @return {"live_replay":[{"expire_time":"","create_time":"","media_url":""}], "errcode": 0, "total": 总数, "errmsg":"ok"}
     */
    public function getLiveInfo($data = [])
    {
        $url = 'https://api.weixin.qq.com/wxa/business/getliveinfo?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 直播间导入商品
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     * @param {"ids": [1, 2]商品id, "roomId": 房间id}
     * @return {"errcode": 0}
     */
    public function addLiveGoods($data = [])
    {
        $url = 'https://api.weixin.qq.com/wxaapi/broadcast/room/addgoods?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 直播间删除商品
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     * @param {"goodsId": 商品id, "roomId": 房间id}
     * @return {"errcode": 0}
     */
    public function deleteLiveGoods($data = [])
    {
        $url = 'https://api.weixin.qq.com/wxaapi/broadcast/goods/deleteInRoom?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 删除直播间
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     * @param {"id": 房间id}
     * @return {"errcode": 0}
     */
    public function deleteLive($data = [])
    {
        $url = 'https://api.weixin.qq.com/wxaapi/broadcast/room/deleteroom?access_token=ACCESS_TOKEN';
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 商品添加并提审
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     * @param {"goodsInfo": {"coverImgUrl": "缩略图", "name":"名称", "priceType":价格1固定2取件, "price":价格1, "price2":价格2, "url":"商品链接"}}
     * @return {"goodsId": 商品id,"auditId": 审核单id,"errcode": 0}
     */
    public function addGoods($data)
    {
        $url = "https://api.weixin.qq.com/wxaapi/broadcast/goods/add?access_token=ACCESS_TOKEN";
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 商品撤回审核
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     * @param {"auditId": 审核单id, "goodsId": 商品id}
     * @return {"errcode": 0}
     */
    public function resetAuditGoods($data)
    {
        $url = "https://api.weixin.qq.com/wxaapi/broadcast/goods/resetaudit?access_token=ACCESS_TOKEN";
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 重新提交审核
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     * @param {"goodsId": 商品id}
     * @return {"auditId": 审核单id,"errcode": 0}
     */
    public function auditGoods($data)
    {
        $url = "https://api.weixin.qq.com/wxaapi/broadcast/goods/audit?access_token=ACCESS_TOKEN";
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 删除商品
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     * @param {"goodsId": 商品id}
     * @return {"errcode": 0}
     */
    public function deleteGoods($data)
    {
        $url = "https://api.weixin.qq.com/wxaapi/broadcast/goods/delete?access_token=ACCESS_TOKEN";
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 更新商品
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     * @param {"goodsInfo": {"coverImgUrl": "缩略图", "name":"名称", "priceType":价格1固定2取件, "price":价格1, "price2":价格2, "url":"商品链接","goodsId": 商品id}}
     * @return {"errcode": 0}
     * @description 审核通过的商品仅允许更新价格类型与价格，审核中的商品不允许更新，未审核的商品允许更新所有字段， 只传入需要更新的字段
     */
    public function updateGoods($data)
    {
        $url = "https://api.weixin.qq.com/wxaapi/broadcast/goods/update?access_token=ACCESS_TOKEN";
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 获取商品状态
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     * @param {'goods_ids' => [1,2,3]} 最多20个
     * @return {"errcode":0,"errmsg":"ok","goods":[{"goods_id":商品id,"cover_img_url":"封面图","name":"名称","price":价格1,"url":"链接","price_type":1固定2区间,"price2":价格2,"audit_status":0未审核1审核中2审核通过3审核失败}],"total":总数}
     */
    public function stateGoods($data)
    {
        $url = "https://api.weixin.qq.com/wxa/business/getgoodswarehouse?access_token=ACCESS_TOKEN";
        return $this->callPostApi($url, $data, true);
    }

    /**
     * 获取商品列表
     * @param array $data
     * @return array
     * @throws InvalidResponseException
     * @throws LocalCacheException
     * @param {'offset' => 开始, 'limit' => 个数最多100, 'status' => 0未审核1审核中2审核通过3审核驳回}
     * @return {"errcode":0,"goods":[{"goods_id":商品id,"cover_img_url":"封面图","name":"名称","price":价格1,"url":"链接","price_type":1固定2区间,"price2":价格2,"audit_status":0未审核1审核中2审核通过3审核失败}],"total":总数}
     */
    public function getGoods($offset = 0, $limit = 100, $status = 0)
    {
        $url = "https://api.weixin.qq.com/wxaapi/broadcast/goods/getapproved?access_token=ACCESS_TOKEN&offset={$offset}&limit={$limit}&status={$status}";
        return $this->callGetApi($url);
    }

}